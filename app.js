var epamBus = angular.module('epamBus', []);

epamBus.controller('TripController', ['$scope', function ($scope) {
        $scope.isSelected = '';
        $scope.destinations = [
            {"city": "Eger",
                "trips": [
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Express bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Intercity bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Intercity bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "12:00",
                        "service": "Express bus service"}
                ]
            },
            {"city": "Miskolc",
                "trips": [
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Intercity bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "12:00",
                        "service": "Tour bus service"}
                ]
            },
            {"city": "Szeged",
                "trips": [
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Intercity bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Express bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "12:00",
                        "service": "Tour bus service"}
                ]
            },
            {"city": "Pécs",
                "trips": [
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Express bus service"},
                    {"departure": "10:30", "arrive": "12:00",
                        "service": "Tour bus service"}
                ]
            },
            {"city": "Debrecen",
                "trips": [
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Intercity bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Bus rapid transit"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "12:00",
                        "service": "Tour bus service"}
                ]
            },
            {"city": "Badacsony",
                "trips": [
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Intercity bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Intercity bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Bus rapid transit"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "12:00",
                        "service": "Tour bus service"}
                ]
            },
            {"city": "Sopron",
                "trips": [
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Intercity bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"},
                    {"departure": "10:30", "arrive": "13:00",
                        "service": "Tour bus service"}
                ]
            }
        ];

        $scope.setValue = function (destination) {
            $scope.currentDestination = destination;
            $scope.isSelected = 'active';
        }
    }]);